<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class animeController extends Controller
{
    //
    public function displayAll(){
        $directory = '/Anime';
        $directories = Storage::allFiles($directory);

        return $directories;
    }
}
