<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class moviesController extends Controller
{
    //
    public function displayAll(){
        $directory = '/Movies';
        $directories = Storage::allFiles($directory);
        
        return $directories;
    }
}
