<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class seriesController extends Controller
{
    //
    public function displayAll(){
        $directory = '/Series';
        $directories = Storage::allFiles($directory);

        return $directories;
    }
}
